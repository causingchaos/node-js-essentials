//These are all core libraries for node.js
var path = require('path');
//console log but with time stamps.
var util = require('util');
// current memory usage using v8. V8 processor is part of how node.js is built.
//on top of chrome's v8 engine.
var v8 = require('v8');


util.log( path.basename(__filename) );

var dirUploads = path.join(__dirname, 'www', 'files','uploads');

util.log(dirUploads);

util.log(v8.getHeapStatistics());


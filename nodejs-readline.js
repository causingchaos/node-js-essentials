var readline = require('readline');

var rl = readline.createInterface(process.stdin, process.stdout);
var realPerson = {
    name: '',
    sayings: []
};

rl.question("What is the name of a real person?", function(answer) {

    realPerson.name = answer;
    rl.setPrompt(`What would ${realPerson.name} say?`);
    rl.prompt();

    //listen for any new lines, event will fire when user submits an answer.
    // then callback function() will be invoked.
    rl.on('line', function(saying) {

        saying = saying.toLowerCase().trim();
        if (saying === 'exit'){
            rl.close();
        }else{
            rl.setPrompt(`What else would ${realPerson.name} say? ('exit' to leave)`);
            rl.prompt();
            realPerson.sayings.push(saying);
        }


    });
});

rl.on('close', function() {
    console.log("%s is a real person that says %j", realPerson.name, realPerson.sayings);
});



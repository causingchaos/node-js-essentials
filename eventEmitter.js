/*
Event emiiter is Node.js's implementation of the pub/sub design pattern, and allows us to create
listners for an emit custom events.
 */

//on function is an example of the event emmiter.

var events = require('events');

//constructor for Event Emitter
var emitter = new events.EventEmitter();

//When a custom event is raised, we will pass a message and a status to this
//callback function asynchronously.
emitter.on('customEvent', function(message, status) {
    console.log(`${status}: ${message}`);
});

emitter.emit('customEvent', "Hello World", 200);

